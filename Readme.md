### How to run?

Make sure you have `node.js` installed.

```bash
npm install
npm start
```

Then go ahead to http://localhost:3000
